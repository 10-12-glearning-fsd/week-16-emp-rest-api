package com.glearning.employeeapi.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
class DBHealthIndicator implements HealthIndicator{

	@Override
	public Health health() {
		boolean flag = false;
		if(flag)
		return Health.up().build();
		else 
			return Health.down().build();
	}
	
}

@Component
class PaymentGatewayHealthIndicator implements HealthIndicator{

	@Override
	public Health health() {
		boolean flag = true;
		if(flag)
		return Health.up().build();
		else 
			return Health.down().build();
	}
	
}
