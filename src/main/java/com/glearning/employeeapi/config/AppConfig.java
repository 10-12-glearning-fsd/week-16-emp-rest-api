package com.glearning.employeeapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	
	@Bean
	public User userBean() {
		return new User();
	}
	@Bean
	public User userBeanWithDefaultConstructor() {
		return new User();
	}

}

class User {
	
}
