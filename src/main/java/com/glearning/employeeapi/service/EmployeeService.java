package com.glearning.employeeapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.employeeapi.dao.EmployeeRepository;
import com.glearning.employeeapi.model.Employee;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public Employee saveEmployee(Employee employee) {
		return this.employeeRepository.save(employee);
	}
	
	public List<Employee> fetchAllEmployees(){
		return this.employeeRepository.findAll();
	}
	
	public Employee findEmployeeById(long empId) {
		return this.employeeRepository.findById(empId).orElseThrow();
	}
	
	public void deleteEmployeeById(long empId) {
		this.employeeRepository.deleteById(empId);
	}

}
