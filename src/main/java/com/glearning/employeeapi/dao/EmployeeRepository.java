package com.glearning.employeeapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.glearning.employeeapi.model.Employee;

/**
 * This interface will talk to the db and perform the CRUD on the database
 * 
 * @author pradeep
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	
}
