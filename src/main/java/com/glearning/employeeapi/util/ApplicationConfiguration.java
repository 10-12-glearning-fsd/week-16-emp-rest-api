package com.glearning.employeeapi.util;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
/*
 * <bean id="applicationConfiguration" class="com.glearning.employeeapi....">
 *   <property name="applicationContext" />
 * </bean>
 */
public class ApplicationConfiguration implements CommandLineRunner {
	
	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void run(String... args) throws Exception {
		//System.out.println("Hello world");
		String[] beanNameArray  = this.applicationContext.getBeanDefinitionNames();
		Stream.of(beanNameArray)
		.filter(bean -> bean.startsWith("user"))
		.forEach(bean -> System.out.println(bean));
	}

}
